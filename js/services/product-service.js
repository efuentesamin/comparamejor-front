/**
 * product-service.js
 * Autor: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 */



 comparamejorApp.factory('ProductService', ['$http',

	function($http) {
		var baseUrl = 'http://localhost:9000/products';

		return {
			getList: function() {
				return $http.get(baseUrl);
			},
			getDetail: function(id) {
				return $http.get(baseUrl + "/" + id);
			}
		};
	}
]);