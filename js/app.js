/*
 * app.js
 * Autor: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 */



//Módulo principal
var comparamejorApp = angular.module('comparamejor', ['ngRoute', 'doowb.angular-pusher']);


comparamejorApp.config(['$routeProvider', '$locationProvider', '$httpProvider', 'PusherServiceProvider',


	function($routeProvider, $locationProvider, $httpProvider, PusherServiceProvider) {

		//Configuraciones generales
        $httpProvider.defaults.useXDomain = true;
		$httpProvider.defaults.headers.common['Accept'] = 'application/json';
		delete $httpProvider.defaults.headers.common['X-Requested-With'];

		//Definición de rutas url
		$routeProvider
			.when('/main', {controller: 'MainController', templateUrl: 'templates/main/main.html'})
			.otherwise({redirectTo: '/main'});

		PusherServiceProvider
			.setToken('e85a0a053bd198b94fa0')
			.setOptions({});
			
		//$locationProvider.html5Mode(true);
	}

]);

