/**
 * main-controller.js
 * Autor: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 */



 comparamejorApp.controller('MainController', ['$scope', 'ProductService', 'Pusher',


	function($scope, ProductService, Pusher) {

		$scope.selectedProduct = null;
		$scope.products = [];
		$scope.prices = [];
		$scope.prices[0] = {store: 1, value: 0, loading: false};
		$scope.prices[1] = {store: 2, value: 0, loading: false};
		$scope.prices[2] = {store: 3, value: 0, loading: false};
		$scope.prices[3] = {store: 4, value: 0, loading: false};
		$scope.prices[4] = {store: 5, value: 0, loading: false};

		Pusher.subscribe('comparamejor', 'priceReceived', function (data) {
			$scope.updatePrice(data.store - 1, data.price);
		});

		ProductService.getList().success(function(data, status, headers, config){
			$scope.products = data;
		});



		$scope.requestPrices = function(){
			console.log($scope.selectedProduct);
			if($scope.selectedProduct !== null){
				for (var i in $scope.prices){
					var price = $scope.prices[i];
					price.loading = true;
				}
				ProductService.getDetail($scope.selectedProduct.id).success(function(data, status, headers, config){
				});
			}else{
				alert('Debe seleccionar un producto.');
			}
		};



		$scope.updatePrice = function(id, value){
			var price = $scope.prices[id];
			price.value = value;
			price.loading = false;
			console.log(price);
		};
	}

]);
